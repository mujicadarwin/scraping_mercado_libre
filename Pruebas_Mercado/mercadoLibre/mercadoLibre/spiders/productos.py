# -*- coding: utf-8 -*-

import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
#from mercadoLibre.items import MercadolibreItem
#from mercadoLibre.mercadoLibre.items import MercadolibreItem
from ..items import MercadolibreItem


class MercadoSpyder(CrawlSpider):
    name = 'mercado'
    item_count = 0
    allowed_domain = ['www.mercadolibre.com.co']
    start_urls = ['https://listado.mercadolibre.com.co/maquinas-peluqueria']


    rules = {

        Rule(LinkExtractor(allow=(),restrict_xpaths=('//*[contains(@class, "pagination__button--next")]/a/@href'))),# regla para que el spider pase a la siguiente pagina
        Rule(LinkExtractor(allow=(),restrict_xpaths=("//a[@class='item__info-title']/@href")),
             callback='parse_item', follow=True)
    }


    def parse_item(self, response):

        mi_item = MercadolibreItem()


        #info de producto

        mi_item['titulo'] = response.xpath('//h1[@class="item-title__primary"]/text()').get()
        #mi_item['titulo'] = response.xpath('normalize-space(//h1[@class="item-title__primary"]/text())').extract()
        #mi_item['precio'] = response.xpath('normalize-space(//*[@id="productInfo"]/fieldset[1]/span/span[2]/text())').extract()
        #mi_item['stock_disponible'] = response.xpath('normalize-space(//*[@id="productInfo"]/div[1]/p/text())').extract()
        #mi_item['condicion'] = response.xpath('normalize-space(//div[@class="item-conditions"]/text())').extract()
        #mi_item['envio'] = response.xpath('normalize-space(//*[contains(@class,"shipping-method-title")]/span/text())').extract()
        #mi_item['opiniones'] = response.xpath('normalize-space(//span[@class="review-summary-average"]/text())').extract() # //*[@id="reviewsCard"]/div/div[1]/span[1]
        #mi_item['ventas_producto'] = response.xpath('normalize-space(//*[@id="short-desc"]/div/dl/div)').extract()
        #mi_item['unds_disponibles'] = response.xpath('normalize-space(//*[@id="dropdown-quantity"]/button/span[4]/text())').extract()
        #mi_item['garantia_producto'] = response.xpath('normalize-space(//*[contains(@class,"warranty__store")]/text())').extract()
        #mi_item['suma_puntos'] = response.xpath('normalize-space(//p[@class="benefit__text"]/text())').extract()
       
        #info vendedor

        #mi_item['url_vendedor'] = response.xpath('normalize-space(//a[@id="seller-view-more-link"]/@href)').extract() # //*[@id="seller-view-more-link"]/@href
        #mi_item['tipo_vendedor'] = response.xpath('normalize-space(//*[@id="root-app"]/div/div[1]/div[2]/div[1]/section[2]/div[2]/p[1]/text())').extract() #
        #mi_item['ubicacion'] = response.xpath('normalize-space(//*[@id="root-app"]/div/div[1]/div[2]/div[1]/section[2]/div[1]/p[2])').extract()
        #mi_item['ventas_vendedor'] = response.xpath('normalize-space(//*[@id="root-app"]/div/div[1]/div[2]/div[1]/section[2]/div[4]/dl/dd[1]/text())').extract() #//dd[@class="reputation

        yield {
            'titulo': mi_item


        }


        """self.item_count += 1
        if self.item_count > 30:
            raise CloseSpider('item_exceeded')
        yield mi_item """
	# verificando los cambios en el commit
